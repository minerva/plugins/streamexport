let assert = require('assert');

it('test version', function () {
  let result;
  global.minerva = {};
  global.minervaDefine = function (param) {
    if (typeof param === 'function') {
      result = param();
    } else {
      result = param;
    }
  }
  require('../dist/plugin')
  assert.equal(result.getVersion(), require('root-require')('package.json').version);
});
