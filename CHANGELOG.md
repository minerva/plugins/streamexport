# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2021-11-22
### Fixed
- Exporting of big chunks of a map crashed (the URL was too long) 

## [1.0.0] - 2020-01-21
### Added
- exporting parts of network upstream and downstream of a node

