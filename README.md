# Minerva plugin for exporting parts of network upstream and downstream of a node

### General instructions

In order to use the precompiled and publicly available version of the plugin, 
open the plugin menu in the MINERVA's upper left corner (see image below) and click plugins.
In the dialog which appears enter the following address in the URL box: `https://minerva-dev.lcsb.uni.lu/plugins/streamexport/plugin.js` .
The plugin shows up in the plugins panel on the right hand side of the screen.

### Plugin functionality

The plugin exports part of a network which is upstream, downstream or both up and downstream
from an element. The plugin allows to:

- Select a stating element from the map
- Select direction
- Select the number of steps (by default unlimited)
- Select the export format
- Select whether process should stop on modifiers