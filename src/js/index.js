require('../css/styles.css');
const zip = require('jszip')
// const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

const concaveman = require('concaveman');
var xmldom = require('xmldom');
var DOMParser = require('xmldom').DOMParser;
var xpath = require('xpath');


const pluginId = 'streamexport';
const pluginName = 'Stream export';
const pluginVersion = '1.0.1';

const globals = {
    selected: [],
    allBioEntities: [],
    pickedRandomly: undefined
};

let $ = window.$;
if ($ === undefined) {
    $ = minerva.$;
}

var Point = function(x, y) {

    this.x = x;
    this.y = y;

    this.plus =  p => new Point(this.x + p.x, this.y + p.y);
    this.minus =  p => new Point(this.x - p.x, this.y - p.y);
    this.size = () => Math.sqrt(this.x *this.x +  this.y *this.y);
    this.normalize = () => {let size = this.size(); return new Point(this.x / size, this.y / size);};
    this.times = m => new Point(this.x * m, this.y * m);
    this.generateNeighbor = (r) => new Point(this.x + Math.random()*r, this.y + Math.random()*r);
};

var Directions = {
    DOWNSTREAM: 0,
    UPSTREAM: 1,
    BOTH: 2,
    properties : {
        labels: ['Downstream', 'Upstream', 'Undirectional']
    }
};

// ******************************************************************************
// ********************* PLUGIN REGISTRATION WITH MINERVA *********************
// ******************************************************************************

let minervaProxy;
let pluginContainer;
let pluginContainerId;


const register = function(_minerva) {

    // console.log('registering ' + pluginName + ' plugin');

    $(".tab-content").css('position', 'relative');

    minervaProxy = _minerva;
    pluginContainer = $(minervaProxy.element);
    pluginContainerId = pluginContainer.attr('id');

    // console.log('minerva object ', minervaProxy);
    // console.log('project id: ', minervaProxy.project.data.getProjectId());
    // console.log('model id: ', minervaProxy.project.data.getModels()[0].modelId);

    initPlugin();
};

const unregister = function () {
    // console.log('unregistering ' + pluginName + ' plugin');

    unregisterListeners();
    return deHighlightAll();
};

const getName = function() {
    return pluginName;
};

const getVersion = function() {
    return pluginVersion;
};

/**
 * Function provided by Minerva to register the plugin
 */
minervaDefine(function (){
    return {
        register: register,
        unregister: unregister,
        getName: getName,
        getVersion: getVersion
        ,minWidth: 400
    }
});

function initPlugin () {
    registerListeners();
    initMainPageStructure();
}

function registerListeners(){
    minervaProxy.project.map.addListener({
        dbOverlayName: "search",
        type: "onSearch",
        callback: searchListener
    });
}

function unregisterListeners() {
    minervaProxy.project.map.removeAllListeners();
}

// ****************************************************************************
// ********************* MINERVA INTERACTION*********************
// ****************************************************************************


function deHighlightAll(){
    return minervaProxy.project.map.getHighlightedBioEntities().then( highlighted => minervaProxy.project.map.hideBioEntity(highlighted) );
}

// ****************************************************************************
// ********************* PLUGIN STRUCTURE AND INTERACTION*********************
// ****************************************************************************

function initMainPageStructure(){

    const container = $('<div class="' + pluginId + '-container"></div>').appendTo(pluginContainer);
    container.append(`        
        <form class="main-form form-horizontal">
            <div class="form-group row">
                <label class="col-sm-3 control-label col-form-label">Starting nodes: </label>
                <div class="col-sm-9">                    
                    <p class="form-control-static input-start">Pick a starting species from the map</p>
                </div>
            </div>            
            <div class="form-group row">
                <label class="col-sm-3 control-label col-form-label">Direction: </label>
                <div class="col-sm-9">
                    <select class="form-control strexp-direction">
                        <option value="0">${Directions.properties.labels[0]}</option>
                        <option value="1">${Directions.properties.labels[1]}</option>
                        <option value="2">${Directions.properties.labels[2]}</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 control-label col-form-label">Modifiers: </label>
                <div class="col-sm-9">
                    <select class="form-control strexp-modifiers">
                        <option value="0">Blocking</option>
                        <option value="1">Non-blocking</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 control-label col-form-label">Steps: </label>
                <div class="col-sm-9">
                    <input type="number" class="input-steps form-control" min="1">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 control-label col-form-labels">Format: </label>
                <div class="col-sm-9">
                    <select class="form-control strexp-format">                        
                    </select>
                </div>
            </div>                
        </form>
        <button type="button" class="btn-export btn btn-primary btn-default btn-block">${exporting(false)}</button>       
        
        <div class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Error</h4>
                    </div>
                    <div class="modal-body">
                        <p class="text-warning">Please select a starting node by clicking an element on the map.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div> 
    `);

    let selectFormat = pluginContainer.find(".strexp-format");
    minervaProxy.configuration.modelConverters
        .forEach((c, i) => selectFormat.append(`<option value="${i}"> ${c.name} </option>`));

    container.find('.btn-export').on('click', () => exportStream() );
}

function updateStartingNodesContainer(){
    let $input = pluginContainer.find('.input-start');
    $input.empty();
    globals.selected.forEach((e, i) => $input.append(`
            <div name="${i}">
                ${e.getName()} 
                <button type="button" name="${i}" class="btn btn-default btn-xs"><span class="fa fa-minus"></span></button> 
            </div>`));

    $input.find("button").click((e) => {
        let name = $(e.target).parent().attr('name');
        $input.find(`div[name="${name}"]`).remove();
        //name is the index
        globals.selected.splice(parseInt(name), 1);
        updateStartingNodesContainer();
        console.log(globals.selected);
    });
}

function searchListener(entities) {
    entities[0].forEach( entity => {
        if (entity.constructor.name === 'Alias') {
            if (globals.selected.indexOf(entity) < 0){
                globals.selected.push(entity)
            }
        }
    });

    updateStartingNodesContainer();



    // if (entities[0].length > 0 && entities[0][0].constructor.name === 'Alias') {
    //     globals.selected = entities[0];
    //
    //     pluginContainer.find('.input-start').html(globals.selected[0].getName());
    // }
}

function ab2str(buf) {
    return String.fromCharCode.apply(null, new Uint16Array(buf));
}

function handler () {
    console.log(this.getResponseHeader('content-type'));
}

function queryApi(query, parameters, queryType) {

    const apiAddress = ServerConnector.getApiBaseUrl();
    if (!queryType) queryType = 'GET';
    if (!parameters) parameters = '';

    let url = `${apiAddress}${query}`;

    let paramString = "";
    for (let key in parameters){
        if (paramString !== "") {
            paramString += "&";
        }
        paramString += `${key}=${parameters[key]}`;
    }
    if (queryType !== "POST") {
        url += `?${paramString}`;
    }


    return new Promise((resolve, reject) => {

        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = handler;
        xhr.open(queryType, url);

        xhr.responseType = 'arraybuffer';

        xhr.onload = () => {

            if (xhr.status === 200) {
                const dataView = new DataView(xhr.response);
                const decoder = new TextDecoder('utf-8');
                const model = decoder.decode(dataView);
                if (model.startsWith("<?xml")) {
                    resolve(model)
                } else {
                    resolve(new Blob([xhr.response]))
                }
                // console.log(model)
                // console.log(new Blob([xhr.response])); // Blob
                // // console.log(xhr.getResponseHeader('Content-Type'))
                // resolve(model)
                // resolve(new Blob([xhr.response]));
            }
            if (xhr.status >= 200 && xhr.status < 300) {
                resolve(xhr.response);
            } else {
                reject(xhr.statusText);
            }
        };

        xhr.onerror = () => reject(xhr.statusText);


        if (queryType === "POST") {
            let params = "";
            for (let paramName in parameters) {
              params += encodeURIComponent(paramName) + '=' + encodeURIComponent(parameters[paramName]) + "&";
            }

            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            // xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
            // xhr.setRequestHeader("Content-length", params.length);
            // xhr.setRequestHeader("Connection", "close");
            xhr.send(params);
        } else {
            xhr.send();
        }


    });

}

// function queryApi(query, parameters, queryType) {
//
//     const apiAddress = ServerConnector.getApiBaseUrl();
//     if (!queryType) queryType = 'GET';
//     if (!parameters) parameters = '';
//
//     let paramString = "";
//     for (let key in parameters){
//         if (paramString !== "") {
//             paramString += "&";
//         }
//         paramString += `${key}=${parameters[key]}`;
//     }
//
//     return $.ajax({
//         type: queryType,
//         url: `${apiAddress}${query}`,
//         // dataType: 'json',
//         // processData: false,
//         data: parameters
//         // data: paramString
//     })
//
// }


function getConcaveHull(species, reactions) {

    if (species.length == 0) return;

    let points = [];

    let extractLineCoordinates = function (lines) {
        return lines.map(l=>[l.start, l.end]);
    };

    let generatePointsAlongLine = function(line) {
        const cntPointsInCloud = 3;
        const cloudRad = 2;

        let p1 = new Point(line[0].x, line[0].y);
        let p2 = new Point(line[1].x, line[1].y);
        let p1p2 = p2.minus(p1);
        let p1p2Normalized = p1p2.normalize();
        let step = p1p2.size() / 10;
        for (let i = step; i <= p1p2.size(); i+=step) {
            let p = p1.plus(p1p2Normalized.times(i));
            points.push([p.x, p.y]);
            for (let j = 0; j < cntPointsInCloud; j++ ) {
                let n = p.generateNeighbor(cloudRad);
                points.push([n.x, n.y]);
            }
        }
    };

    species.concat(reactions).forEach(function (entity) {
        const centerX = entity.getCenter().x;
        const centerY = entity.getCenter().y;
        points.push([centerX, centerY]);
    });

    // reactions.forEach(function (r) {
    //     let lines = extractLineCoordinates(r.startLines)
    //         .concat(extractLineCoordinates(r.midLines))
    //         .concat(extractLineCoordinates(r.endLines));
    //
    //     lines.forEach(l => generatePointsAlongLine(l));
    // });

    // console.log("points", points);

    return concaveman(points, 500);
    // return hull(points, 10);
}

function getPolygonString(points){
    return points.map(p => `${p[0]},${p[1]}`).join(";");
}

function storeToFile(fileName, content) {

    let blob = new Blob([content], { type: 'text/xml;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, fileName);
    } else {
        let link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", fileName);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function getNeighbors(bioEntities, direction, steps, modsBlock, result, result_reactions) {

    if (!result) result = [];
    if (!result_reactions) result_reactions = [];

    const intersection = function(entities, r_entities) {
        for (let i =0; i < entities.length; i++) {
            if (r_entities.indexOf(entities[i]) >= 0){
                return true;
            }
        }
        return false;
    };

    return fetchBioObjects(bioEntities, 'REACTION').then(function (reactions) {

        // console.log("reactions", reactions);
        const relevant_reactions = reactions.filter(r => {
            let relevant = false;
            const p_entities = r.getProducts().map(p => p.getAlias());
            const r_entities = r.getReactants().map(p => p.getAlias());
            const m_entities = r.getModifiers().map(p => p.getAlias());

            if (direction == Directions.DOWNSTREAM) {
                return (intersection(bioEntities, r_entities) || !modsBlock && intersection(bioEntities, m_entities))
            }
            if (direction == Directions.UPSTREAM) {
                return intersection(bioEntities, p_entities)
            }
            if (direction == Directions.BOTH ) {
                return intersection(bioEntities, r_entities) || intersection(bioEntities, p_entities)
            }
            // if (direction !== Directions.DOWNSTREAM) {
            //     relevant = relevant || intersection(bioEntities, p_entities);
            // }
            // if (direction !== Directions.UPSTREAM) {
            //     relevant = relevant || intersection(bioEntities, r_entities);
            // }
            // if (direction == Directions.BOTH && !modsBlock) {
            //     relevant = relevant || intersection(bioEntities, m_entities);
            // }
            return relevant;
        });

        // console.log("relevant_reactions", relevant_reactions);
        // console.log("relevant_reactions", relevant_reactions.map(r=>r.getId()));

        var entities = [];
        let entities_to_follow = [];

        relevant_reactions.forEach(function (r) {
            entities = entities.concat(r.getReactants().map(p => p.getAlias()));
            entities = entities.concat(r.getProducts().map(p => p.getAlias()));
            entities = entities.concat(r.getModifiers().map(p => p.getAlias()));

            // if (direction !== Directions.DOWNSTREAM) {
            //     entities_to_follow = entities_to_follow.concat(r.getReactants().map(p => p.getAlias()))
            // }
            // if (direction !== Directions.UPSTREAM) {
            //     entities_to_follow = entities_to_follow.concat(r.getProducts().map(p => p.getAlias()))
            // }
            // if (!modsBlock) {
            //     entities_to_follow = entities_to_follow.concat(r.getModifiers().map(p => p.getAlias()))
            // }
            if (direction == Directions.DOWNSTREAM || direction == Directions.BOTH) {
                entities_to_follow = entities_to_follow.concat(r.getProducts().map(p => p.getAlias()))
                // let names = r.getProducts().map(p => p.getAlias().getName());
                // if (names.indexOf('NFKB/N') >= 0) {
                //     console.log('NFKB/N reaction', r);
                // }

            }
            if (direction == Directions.UPSTREAM || direction == Directions.BOTH) {
                entities_to_follow = entities_to_follow.concat(r.getReactants().map(p => p.getAlias()))
                if (!modsBlock) {
                    entities_to_follow = entities_to_follow.concat(r.getModifiers().map(p => p.getAlias()))
                    }

            }
            // if (!modsBlock) {
            //     entities_to_follow = entities_to_follow.concat(r.getModifiers().map(p => p.getAlias()))
            // }
        });

        entities = entities.filter(e => result.indexOf(e) < 0);
        entities_to_follow = entities_to_follow.filter(e => result.indexOf(e) < 0);
        entities = entities.filter(onlyUnique);
        entities_to_follow = entities_to_follow.filter(onlyUnique);


        // console.log("entities_to_follow", entities_to_follow.map(e => e.getName()));

        result = result.concat(entities);
        result_reactions = result_reactions.concat(relevant_reactions);
        result_reactions = result_reactions.filter(onlyUnique);

        // console.log("result", result);
        // console.log("result_reactions", result_reactions);

        return entities_to_follow.length === 0 || steps == 0 ?
            {species: result, reactions: result_reactions} :
            getNeighbors(entities_to_follow, direction, steps-1, modsBlock, result, result_reactions);
    });
}

function getSpeciesAliasSpeciesDict(doc) {
    let dict = {};
    xpath.select('//speciesAlias', doc).forEach(sa => {
        let saS = sa.getAttribute('species');
        if (!(saS in dict)) dict[saS] = [];
        dict[saS].push(sa);
    });
    return dict;

}

function getSpeciesAliasComplexSpeciesAliasDict(doc) {
    let dict = {};
    xpath.select('//speciesAlias', doc).forEach(sa => {
        let saCSA = sa.getAttribute('complexSpeciesAlias');
        if (saCSA != "") {
            if (!(saCSA in dict)) dict[saCSA] = [];
            dict[saCSA].push(sa);
        }

    });
    return dict;
}

function getComplexSpeciesAliasSpeciesAliasDict(doc) {
    let dict = {};
    xpath.select('//complexSpeciesAlias', doc).forEach(csa => {
        let sCSA = csa.getAttribute('species');
        if (sCSA != "") {
            if (!(sCSA in dict)) dict[sCSA] = [];
            dict[sCSA].push(csa);
        }

    });
    return dict;
}

function filterModel(model, species, reactions) {

    let sbmlTagRE = /<sbml[^>]*>/;
    let sbmlTagMatch = model.match(sbmlTagRE); //invalid namespace causes issues in xpath

    var doc = new DOMParser().parseFromString(model.replace(sbmlTagRE, '<sbml>'));
    // var doc = new DOMParser().parseFromString(model);

    //REMOVE REACTIONS

    const reacionIds = reactions.map(r => r.getReactionId());

    // console.log("removing reactions");

    let rElements = doc.getElementsByTagName('reaction');
    var toRemove = [];
    for (let i  = 0; i < rElements.length; i++) {
        let r = rElements[i];
        // xpath.select("//reaction", doc).forEach(r => {
        if (reacionIds.indexOf(r.getAttribute('id')) < 0) {
            toRemove.push(r);
        }
    }
    // });

    for (let i = 0 ; i < toRemove.length; i++) {
        let r = toRemove[i];
        r.parentNode.removeChild(r)
    }

    //REMOVE SPECIES

    const speciesIds = species.map(s => s.getElementId());

    // console.log("removing nonb-complex species");

    // remove aliases of species not from complexes
    let saElements = doc.getElementsByTagName('celldesigner:speciesAlias');
    toRemove = [];
    for (let i  = 0; i < saElements.length; i++) {
        let s = saElements[i];
        // xpath.select("//speciesAlias", doc).forEach(s => {
        let saId = s.getAttribute('id');

        //if (speciesIds.indexOf(saId) >= 0) return;
        if (speciesIds.indexOf(saId) >= 0) continue;

        let csId = s.getAttribute('complexSpeciesAlias');
        if (csId == "") {
            toRemove.push(s);
            // s.parentNode.removeChild(s);
        }
    }
    // });
    for (let i = 0 ; i < toRemove.length; i++) {
        let r = toRemove[i];
        r.parentNode.removeChild(r)
    }

    // console.log("removing omplex species 1");

    //remove non-complex species without remaining aliases
    let sToSA = getSpeciesAliasSpeciesDict(doc);
    xpath.select("//listOfSpecies/species", doc).forEach(s => {
        let id = s.getAttribute('id');
        // console.log("removing omplex species 1a", id);
        let cls = s.getElementsByTagName('celldesigner:class');
        if (cls.length > 0 && cls[0].textContent != "COMPLEX") {
        // if (xpath.select(`//listOfSpecies/species[@id="${id}"]//class[.="COMPLEX"]`, doc).length === 0) {
        //     console.log("removing omplex species 1b");
            if (!(id in sToSA)){
            // if (xpath.select(`//speciesAlias[@species="${id}"]`, doc).length === 0) {
            //     console.log("removing omplex species 1c");
                s.parentNode.removeChild(s);
            }
        }
    });

    // console.log("removing omplex species 2");

    //remove complexes - first remove the complexes aliases
    let csaToSA = getSpeciesAliasComplexSpeciesAliasDict(doc);
    xpath.select("//complexSpeciesAlias", doc).forEach(csa => {
        let csaId = csa.getAttribute('id');

        if (speciesIds.indexOf(csaId) >= 0) return;

        csa.parentNode.removeChild(csa);
        // console.log("removing omplex species 2a", csaId);
        csaToSA[csaId].forEach(sa => {
        // xpath.select(`//speciesAlias[@complexSpeciesAlias="${csaId}"]`, doc).forEach(sa => {

            sa.parentNode.removeChild(sa);
        });
    });

    // console.log("removing omplex species 3");

    //remove the complex species if it does not have any more alias connected to it and species included in the removed complexes
    let sToCSA = getComplexSpeciesAliasSpeciesAliasDict(doc);
    xpath.select("//listOfSpecies/species", doc).forEach(s => {
        let sId = s.getAttribute('id');

        // console.log("removing omplex species 3a", sId);
        let cls = s.getElementsByTagName('celldesigner:class');
        if (cls.length > 0 && cls[0].textContent == "COMPLEX") {
        // if (xpath.select(`//listOfSpecies/species[@id="${sId}"]//class[.="COMPLEX"]`, doc).length > 0) {

            if (!(sId in sToCSA)){
            // if (xpath.select(`//complexSpeciesAlias[@species="${sId}"]`, doc).length === 0) {
                //remove the species corrsponding to the complex
                s.parentNode.removeChild(s);
                //remove all included species
                xpath.select(`//listOfIncludedSpecies/species`, doc).forEach(is => {
                    let isId = is.getAttribute('id');
                    // console.log("removing omplex species 3b", isId);
                    let cs = is.getElementsByTagName('celldesigner:complexSpecies');
                    if (cs.length > 0 && cs[0].textContent == sId) {
                    // if (xpath.select(`//listOfIncludedSpecies/species[@id="${isId}"]//complexSpecies[.="${sId}"]`, doc).length > 0) {
                        is.parentNode.removeChild(is);
                    }
                })
            }
        }
    });

    // console.log("Finished");

    // return new xmldom.XMLSerializer().serializeToString(doc);
    return new xmldom.XMLSerializer().serializeToString(doc).replace("<sbml>", sbmlTagMatch[0]).replace('xmlns:celldesigner=""', '');
}

function exporting(inProgress) {
    return inProgress ?
        '<i class="fa fa-circle-o-notch fa-spin"></i> Exporting' :
        'Export';
}

function activateExportBtton(activate) {
    pluginContainer.find('.btn-export').html(exporting(activate));
}

function showModal() {
    pluginContainer.find(".modal").modal('show');
}

function fixFontSizeIssue(str){
    // MINERVA ( <= 12.3.0) exports CellDesigner font size as <celldesigner:font size="30.0"/> but CellDesigner
    // accepts only <celldesigner:font size="30"/> (i.e. without the decimals)

    const re=/(<celldesigner:font size="30)\.[0-9]*("\/>)/g;
    return str.replace(re, '$1$2')
}


function exportStream() {

    if (globals.selected.length === 0) {
        showModal();
        return;
    }

    activateExportBtton(true);

    const projectId = minervaProxy.project.data.getProjectId();
    const modelId = globals.selected[0].getModelId();
    const startName =  globals.selected.map(e=>e.getName()).join('_');

    const startBioEntities = globals.selected;
    let cntSteps = pluginContainer.find('.input-steps').val();
    cntSteps = cntSteps == "" ? -1 : parseInt(cntSteps);
    const modsBlock = parseInt(pluginContainer.find('.strexp-modifiers').val()) === 0;
    const direction = parseInt(pluginContainer.find('.strexp-direction').val());
    const converter = minervaProxy.configuration.modelConverters[parseInt(pluginContainer.find('.strexp-format').val())];
    const handlerClass = converter.handler;
    const fileSuffix = converter.extension;

    getNeighbors(startBioEntities, direction, cntSteps-1, modsBlock,  startBioEntities).then(function (data) {
        // console.log("getNeighbors data", data);
        // let hull = getConcaveHull(data.species, data.reactions);

        const queryParams = {
            handlerClass: handlerClass,
            // polygonString: getPolygonString(hull)
            elementIds: data.species.map(s => s.id).join(','),
            reactionIds: data.reactions.map(r => r.id).join(',')
        };

        queryApi(`projects/${projectId}/models/${modelId}:downloadModel`, queryParams, 'POST').then((response) => {
            if (response instanceof Blob) {
                zip.loadAsync(response).then(zip => {
                    const fileName = `export_${startName}_${Directions.properties.labels[direction]}_steps-${cntSteps}_modblock-${modsBlock}.${fileSuffix}`.toLowerCase();
                    zip.file("model.xml").async("string").then(content => {
                        storeToFile(fileName, content);
                        activateExportBtton(false);
                    })

                }, error => {
                    console.log(error)
                })
            } else {
                const fileName = `export_${startName}_${Directions.properties.labels[direction]}_steps-${cntSteps}_modblock-${modsBlock}.${fileSuffix}`.toLowerCase();
                storeToFile(fileName, response);
                activateExportBtton(false);
            }
        }, (error) => {
            console.log("error", error);
            activateExportBtton(false);
        })
    });
}

function createQuery(entity) {
    return {
        id :  entity.getId(),
        modelId : entity.getModelId(),
        type : "ALIAS"
    }
}

function fetchBioObjects(entities, type) {

    const queryArray =  entities.map(entity => createQuery(entity));

    let promise;
    if (type === "ALIAS") promise = minervaProxy.project.data.getBioEntityById(queryArray);
    else if (type === "REACTION") promise = minervaProxy.project.data.getReactionsWithElement(queryArray);
    else assert(false);

    return promise.then(function(results){
        return results;
    }).catch(function(d){
        console.error("Caught error when fetching bioentities", queryArray);
    });
}